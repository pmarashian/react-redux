import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled, { keyframes } from 'styled-components';
import logo from './logo.svg';

const AppWrapper = styled.div`
	text-align: center;
`;

const Header = styled.header`
	background-color: #222;
  height: 150px;
  padding: 20px;
  color: white;
`;

const LogoAnimation = keyframes`
	from { transform: rotate(0deg); }
	to { transform: rotate(360deg); }
`;

const Logo = styled.img`
	animation: ${LogoAnimation} infinite ${ props => props.duration }s linear;
  height: 80px;
`;

Logo.defaultProps = {
	duration: 20,
};

Logo.propTypes = {
	duration: PropTypes.number,
};

const Title = styled.h1`
	font-size: 1.5em;
	:hover {
		color: darkgoldenrod;
	}
`;

const Intro = styled.p`
	font-size: large;
	code {
		user-select: none;
	}
`;

class App extends Component {
  render() {
    return (
      <AppWrapper>
        <Header>
          <Logo duration={20} src={logo} alt="logo" />
          <Title>Welcome to React</Title>
        </Header>
        <Intro>
          To get started, edit <code>src/App.js</code> and save to reload.
        </Intro>
      </AppWrapper>
    );
  }
}

export default App;
